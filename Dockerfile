FROM node:12.18.1
 
WORKDIR /app
  
RUN npm install express
 
COPY  hello-world-node .

CMD [ "npm", "start" ]
